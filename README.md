# Release manager assignment

Release manager application assignment.

There is docker file in /test/resources/docker that inits Postgres database and populate it with tables. 
It can be run with command:
- docker-compose up postgres

Build info:
- project can be build with _mvn clean package_ and run by _java -jar release.manager.assignment-0-SNAPSHOT.jar_