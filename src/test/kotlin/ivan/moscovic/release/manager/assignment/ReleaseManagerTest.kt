package ivan.moscovic.release.manager.assignment

import ivan.moscovic.release.manager.assignment.controller.Service
import ivan.moscovic.release.manager.assignment.service.ReleaseManagerService
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName


/**
 * To be honest, this test took me definitely more than 3 hours to set up,
 * but I was curious how it works, and now I had time+opportunity to try it
 */
@SpringBootTest(classes = [ReleaseManagerApplication::class])
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Testcontainers
class ReleaseManagerTest {

    @Autowired
    private lateinit var releaseManagerService: ReleaseManagerService

    companion object {
        @Container
        val postgresSQLContainer: PostgreSQLContainer<*> = PostgreSQLContainer(DockerImageName.parse("postgres:latest"))
            .withInitScript("docker/init.sql")

        @DynamicPropertySource
        @JvmStatic
        fun registerDynamicProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", postgresSQLContainer::getJdbcUrl)
            registry.add("spring.datasource.username", postgresSQLContainer::getUsername)
            registry.add("spring.datasource.password", postgresSQLContainer::getPassword)
        }
    }

    @Test
    fun `test deploy`() {
        val resultDeployServiceA1 = releaseManagerService.deployService(Service("Service A", 1))
        Assertions.assertThat(resultDeployServiceA1).isEqualTo(1)
        val resultDeployServiceB1 = releaseManagerService.deployService(Service("Service B", 1))
        Assertions.assertThat(resultDeployServiceB1).isEqualTo(2)
        val resultDeployServiceA2 = releaseManagerService.deployService(Service("Service A", 2))
        Assertions.assertThat(resultDeployServiceA2).isEqualTo(3)
        //System downgrade
        val resultDeployServiceA1Again = releaseManagerService.deployService(Service("Service A", 1))
        Assertions.assertThat(resultDeployServiceA1Again).isEqualTo(2)
        val resultDeployServiceC1 = releaseManagerService.deployService(Service("Service C", 1))
        Assertions.assertThat(resultDeployServiceC1).isEqualTo(4)
        //Service A v2 already exists, but this time, there is 'Service C' deployed too (which was not there
        // when Service A v2 was deployed initially), which means System Version must be 5
        val resultDeployServiceA2Again = releaseManagerService.deployService(Service("Service A", 2))
        Assertions.assertThat(resultDeployServiceA2Again).isEqualTo(5)

        val sysV1 = releaseManagerService.getServicesBySystemVersion(1)
        Assertions.assertThat(sysV1).hasSize(1)
        Assertions.assertThat(sysV1[0].name).isEqualTo("Service A")
        Assertions.assertThat(sysV1[0].version).isEqualTo(1)

        val sysV2 = releaseManagerService.getServicesBySystemVersion(2)
        Assertions.assertThat(sysV2).hasSize(2)
        Assertions.assertThat(sysV2).contains(Service("Service A", 1))
        Assertions.assertThat(sysV2).contains(Service("Service B", 1))

        val sysV5 = releaseManagerService.getServicesBySystemVersion(5)
        Assertions.assertThat(sysV5).hasSize(3)
        Assertions.assertThat(sysV5).contains(Service("Service A", 2))
        Assertions.assertThat(sysV5).contains(Service("Service B", 1))
        Assertions.assertThat(sysV5).contains(Service("Service C", 1))

        val sysV6 = releaseManagerService.getServicesBySystemVersion(6)
        Assertions.assertThat(sysV6).isEmpty()
    }
}