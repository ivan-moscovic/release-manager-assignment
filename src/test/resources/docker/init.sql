create table system_version (
    id             SERIAL,
    version_number SERIAL,
    is_deployed    boolean,
    primary key (id)
);

create table service (
    id                SERIAL,
    name              varchar   not null,
    version_number    int       not null,
    primary key (id),
    unique (name, version_number)
);


create table system_version_service (
    fk_system_version SERIAL,
    fk_service SERIAL,
    constraint fk_system_version_constraint foreign key (fk_system_version) references system_version (id),
    constraint fk_service_constraint foreign key (fk_service) references service (id)
);