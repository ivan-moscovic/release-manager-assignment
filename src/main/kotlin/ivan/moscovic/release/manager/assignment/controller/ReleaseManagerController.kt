package ivan.moscovic.release.manager.assignment.controller

import ivan.moscovic.release.manager.assignment.persistance.ServiceEntity
import ivan.moscovic.release.manager.assignment.service.ReleaseManagerService
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ReleaseManagerController(
    private val releaseManagerService: ReleaseManagerService
) {

    private val log = KotlinLogging.logger { }

    @GetMapping("/services")
    @ResponseBody
    fun getServicesWithSystemVersion(@RequestParam(value = "systemVersion", required = true) systemVersion: Int): ResponseEntity<List<Service>> {
        log.debug("Incoming request: $systemVersion")

        if (systemVersion <= 0) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }

        val response = releaseManagerService.getServicesBySystemVersion(systemVersion)
        log.debug("Response is: $response")
        return ResponseEntity(response, HttpStatus.OK)
    }

    @PostMapping("/deploy")
    @ResponseBody
    fun deployService(@RequestBody service: Service): ResponseEntity<Int> {
        log.debug("Incoming request: $service")

        if (service.version <= 0) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }

        val response = releaseManagerService.deployService(service)
        log.debug("Response is: $response")
        return ResponseEntity(response, HttpStatus.OK)
    }
}

data class Service (
    val name : String,
    val version : Int
)

fun Service.toPersistenceModel() = ServiceEntity(
    serviceName = name,
    versionNumber = version
)