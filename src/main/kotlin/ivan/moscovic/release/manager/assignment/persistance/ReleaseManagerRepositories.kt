package ivan.moscovic.release.manager.assignment.persistance

import org.springframework.data.jpa.repository.EntityGraph
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ServiceRepository : JpaRepository<ServiceEntity, Long> {

    @EntityGraph(value = "serviceGraph")
    fun findByServiceNameAndVersionNumber(serviceName: String, versionNumber: Int) : ServiceEntity?
}

@Repository
interface SystemVersionRepository : JpaRepository<SystemVersionEntity, Long> {

    @EntityGraph(value = "systemVersionGraph")
    fun findByVersionNumber(versionNumber: Int) : SystemVersionEntity?

    @EntityGraph(value = "systemVersionGraph")
    fun findByIsDeployed(isDeployed: Boolean) : SystemVersionEntity?
}