package ivan.moscovic.release.manager.assignment.persistance

import ivan.moscovic.release.manager.assignment.controller.Service
import org.hibernate.annotations.Generated
import org.hibernate.annotations.GenerationTime
import javax.persistence.*

@Entity(name = "system_version")
@NamedEntityGraph(name = "systemVersionGraph",
    attributeNodes = [NamedAttributeNode(value = "services")]
)
class SystemVersionEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,
    @Generated(GenerationTime.INSERT)
    @Column(name = "version_number")
    val versionNumber: Int = 0,
    @Column(name = "is_deployed")
    var isDeployed: Boolean = true,
    @ManyToMany(cascade = [CascadeType.MERGE, CascadeType.PERSIST])
    @JoinTable(name = "systemVersion_service",
        joinColumns = [JoinColumn(name = "fk_system_version")],
        inverseJoinColumns = [JoinColumn(name = "fk_service")])
    val services: List<ServiceEntity>
)

@Entity(name = "service")
@NamedEntityGraph(name = "serviceGraph",
    attributeNodes = [NamedAttributeNode(value = "systemVersionEntities")]
)
class ServiceEntity (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,
    @Column(name = "name")
    val serviceName: String,
    @Column(name = "version_number")
    val versionNumber: Int,
    @ManyToMany(mappedBy = "services")
    val systemVersionEntities: List<SystemVersionEntity> = listOf()
)

fun ServiceEntity.toService() = Service(
    name = serviceName,
    version = versionNumber
)