package ivan.moscovic.release.manager.assignment.service

import ivan.moscovic.release.manager.assignment.controller.Service
import ivan.moscovic.release.manager.assignment.controller.toPersistenceModel
import ivan.moscovic.release.manager.assignment.persistance.*
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
@Transactional
class ReleaseManagerService(
    private val serviceRepository: ServiceRepository,
    private val systemVersionRepository: SystemVersionRepository
) {

    fun getServicesBySystemVersion(systemVersionRequest: Int): List<Service> {
        return systemVersionRepository.findByVersionNumber(versionNumber = systemVersionRequest)
            ?.let { it -> it.services.map { it.toService() } } ?: listOf()
    }

    fun deployService(serviceToDeploy: Service) : Int {

        val serviceFromDb = serviceRepository.findByServiceNameAndVersionNumber(
            serviceName = serviceToDeploy.name,
            versionNumber = serviceToDeploy.version
        )
        val deployedSystemVersion = systemVersionRepository.findByIsDeployed(true)

        return if (serviceFromDb != null) { //service was already deployed before
            processAlreadyDeployedServiceVersion(deployedSystemVersion, serviceFromDb, serviceToDeploy)
        } else { //new service version
            processNewServiceVersion(deployedSystemVersion, serviceToDeploy)
        }
    }

    private fun processAlreadyDeployedServiceVersion(
        deployedSystemVersion: SystemVersionEntity?,
        serviceFromDb: ServiceEntity,
        serviceToDeploy: Service
    ): Int {
        val actualSystemContainsRequestService = deployedSystemVersion!!.services.find {
            it.serviceName == serviceFromDb.serviceName
                    && it.versionNumber == serviceFromDb.versionNumber
        }
        return if (actualSystemContainsRequestService != null) {
            deployedSystemVersion.versionNumber
        } else { //service is not found in current deployed system, it must have been part of some previous release

            //previousVersion must contain all services from current deployed system + serviceFromDb
            val deployedSystemWithoutActualService = deployedSystemVersion.services.filter {
                it.serviceName != serviceFromDb.serviceName
            }
            val previousVersion = serviceFromDb.systemVersionEntities.find { sysVersion ->
                sysVersion.services.containsAll(deployedSystemWithoutActualService) && sysVersion.services.contains(serviceFromDb)
            }

            if (previousVersion == null) {
                deployAsNewSystemVersion(deployedSystemVersion, serviceToDeploy, serviceFromDb)
            } else {

                previousVersion.isDeployed = true
                systemVersionRepository.save(previousVersion)
                deployedSystemVersion.isDeployed = false
                systemVersionRepository.save(deployedSystemVersion)

                previousVersion.versionNumber
            }
        }
    }

    private fun processNewServiceVersion(
        deployedSystemVersion: SystemVersionEntity?,
        serviceToDeploy: Service
    ) = if (deployedSystemVersion == null) {
        val systemVersion = systemVersionRepository.save(
            SystemVersionEntity(services = listOf(serviceToDeploy.toPersistenceModel()))
        )
        systemVersion.versionNumber
    } else {
        deployAsNewSystemVersion(deployedSystemVersion, serviceToDeploy, null)
    }

    private fun deployAsNewSystemVersion(
        deployedSystemVersion: SystemVersionEntity,
        serviceToDeploy: Service,
        serviceFromDb: ServiceEntity?
    ): Int {
        val servicesOnNewSystemVersion = buildList {
            addAll(deployedSystemVersion.services.filter { it.serviceName != serviceToDeploy.name })
            if (serviceFromDb == null) {
                add(serviceToDeploy.toPersistenceModel())
            } else {
                add(serviceFromDb)
            }
        }

        val systemVersion = systemVersionRepository.save(
            SystemVersionEntity(services = servicesOnNewSystemVersion)
        )
        deployedSystemVersion.isDeployed = false
        systemVersionRepository.save(deployedSystemVersion)

        return systemVersion.versionNumber
    }
}